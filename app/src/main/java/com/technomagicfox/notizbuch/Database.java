package com.technomagicfox.notizbuch;

import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;

public class Database {

    static SQLiteDatabase database;
    static Cursor target;

    //CORE
    public static void Initiate() {
        String Path = Constants.DatabasePath + Constants.DatabaseName; //Путь к базе данных
        Log.d("Database", "Path: " + Path);

        try {
            if (database == null && checkStatus(Path)) {
                database = SQLiteDatabase.openOrCreateDatabase(Path, null);
                Log.d("Database", "найден и был присвоен");
                CreateTables();

            } else {
                database = SQLiteDatabase.openOrCreateDatabase(Path, null);
                Log.d("Database", "была создана");
                CreateTables();
                //AddGuideNotes();
            }

        }catch (Exception e){
            Log.e("Database", e.toString());
        }
    }//Основной метод для работы с БД - проверяет наличие, создает/открывает БД

    private static void CreateTables(){
        //Если таблиц нет - создаем их вне зависимости от того - была ли до этого база данный в памяти или нет.
        database.execSQL("CREATE TABLE IF NOT EXISTS notes (id INTEGER, date REAL, title TEXT, body TEXT, tags TEXT, checklist INTEGER)");
        Log.d("Database", "notes созданы если их не было");
        database.execSQL("CREATE TABLE IF NOT EXISTS tags_names (id INTEGER, title TEXT)");
        Log.d("Database", "tags_names созданы если их не было");
        database.execSQL("CREATE TABLE IF NOT EXISTS tags_table (id INTEGER, note_id INTEGER, tag_id INTEGER)");
        Log.d("Database", "tags_table созданы если их не было");
    }

    private static void AddGuideNotes(){
        WriteNote( new Note(
                -1,
                Float.parseFloat(new SimpleDateFormat("yyyyMMddHHmmss").format(Calendar.getInstance().getTime())),
                "Hello, World!",
                "",
                ";",
                0)
        );
        Log.d("Database","Первые заметки добавлены!");
    }

    //SET
    public static void WriteNote(Note note){
            try{
                if(database == null){
                    Initiate();
                }
                //Если id == -1 то значит заметка новая, и ее нужно записать последним возможным номером + 1, а не перезаписать
                if(note.id == -1){
                    note.id = GetLastNoteId() + 1;
                }else {
                    database.delete("notes", "id = " + note.id, null);
                }

                database.execSQL("INSERT INTO notes VALUES ( " +
                        + note.id + ", "
                        + note.date +", '"
                        + note.title + "', '"
                        + note.body + "', '"
                        + note.tags + "', "
                        + note.checklist + ");");

                Log.d("Database","note " + note.title + " added!");
            }
            catch (Throwable t){
                Log.d("Database","note " + note.title + " is NOT added!");
                Log.d("Database",t.toString());
            }
    }//Записывает заметку в базу данных

    public static void DeleteNote(int Number){
        if(Number != -1) {
            database.delete("notes", "id = " + Number, null);//Из таблицы заметок
            database.delete("tags_table", "note_id = " + Number, null);//Из таблицы тегов
        }
    }//Удаляет запись из базы данных

    public static void AddTag(String Name){
        try{
            if(database == null){
                Initiate();
            }

            int newID = GetLastTagId() + 1;

            database.execSQL("INSERT INTO tags_names VALUES ( " +
                    + newID + ", '"
                    + Name + "');");

            Log.d("Database", "Tag " + Name + " added!");
        }catch (Exception e){
            Log.e("Database", e.toString());
        }
    }//Добавить тег

    public static void WriteTagList(TagList tagList){
        try{
            if(database == null){
                Initiate();
            }

            if(tagList.NoteNumber == -1){
                tagList.NoteNumber = GetLastNoteId();
            }else {
                database.delete("tags_table", "note_id = " + tagList.NoteNumber, null);
            }

            for (int TagNumber: tagList.TagsList) {
                if(!isTagTableContains(tagList.NoteNumber, TagNumber)) {
                    database.execSQL("INSERT INTO tags_table VALUES ( " +
                            +GetLastTagTableId() + ", "
                            + tagList.NoteNumber + ", "
                            + TagNumber + ");");
                    Log.d("Database", "tag " + GetLastTagTableId() + ": " + tagList.NoteNumber + "/" + TagNumber + " added");
                }else {
                    Log.d("Database", "tag " + GetLastTagTableId() + ": " + tagList.NoteNumber + "/" + TagNumber + " exist");
                }
            }

        }
        catch (Throwable t){
            Log.d("Database","tag " + tagList.NoteNumber + " is NOT added!");
            Log.d("Database",t.toString());
        }
    }//Присвоить теги для заметки

    public static void DeleteEmptyTags(){
        if(database == null){
            Initiate();
        }

        Tag[] Tags = GetAllTags();

        for(int x = 0; x < Tags.length; x++) {
            if ((int) DatabaseUtils.queryNumEntries(database, "tags_table", "tag_id = " + String.valueOf(Tags[x].Number)) == 0) {
                database.delete("tags_names", "id = " + String.valueOf(Tags[x].Number), null);//Из таблицы тегов
            }
        }
    }

    //GET
    public static Note[] GetAllNotes(){
        Note[] notes = new Note[0];

        if (database == null){
            Initiate();
        }
        target = database.query("notes", null, null, null, null, null, "id");

        if(target.moveToFirst()){
            do{

                notes = Arrays.copyOf(notes, notes.length + 1);
                notes[notes.length -1] = new Note(
                        target.getInt(0),
                        target.getFloat(1),
                        target.getString(2),
                        target.getString(3),
                        target.getString(4),
                        target.getInt(5)
                );


                Log.d("Database.notes", "=============================================");
                Log.d("Database.notes",notes[notes.length - 1].id + "");
                Log.d("Database.notes",notes[notes.length - 1].date + "");
                Log.d("Database.notes",notes[notes.length - 1].title);
                Log.d("Database.notes",notes[notes.length - 1].body);
                Log.d("Database.notes",notes[notes.length - 1].tags);
                Log.d("Database.notes",notes[notes.length - 1].checklist + "");

            }
            while(target.moveToNext());
        }

        return notes;
    }//Читает ВСЕ заметки из базы данных

    public static TagList GetTagsListWithNoteNumber(int Number){
        if(database == null){
            Initiate();
        }
        int[] tagsList = new int[0];

        target = database.query("tags_table", null, "note_id = " + String.valueOf(Number - 1), null, null, null, null);
        if(target.moveToFirst()){
            do{
                tagsList = Arrays.copyOf(tagsList, tagsList.length + 1);
                tagsList[tagsList.length - 1] = target.getInt(2);
                Log.d("GetTagsList", Number + ": " + String.valueOf(tagsList[tagsList.length-1]));
            }
            while(target.moveToNext());
        }
        Log.d("GetTagsList", Number + ": legnth is " + String.valueOf(tagsList.length));
        return new TagList(Number, tagsList);
    }//Получаем список тегов

    public static Tag[] GetAllTags(){
        Tag[] Tags = new Tag[0];

        if(database == null){
            Initiate();
        }

        target = database.query("tags_names", null, null, null, null, null, "id");

        if(target.moveToFirst()){
            do{

                Tags = Arrays.copyOf(Tags, Tags.length + 1);
                Tags[Tags.length -1] = new Tag(
                        target.getInt(0),
                        target.getString(1)
                );

            }
            while(target.moveToNext());
        }


        return Tags;
    }//Читает ВСЕ теги из базы данных

    public static int GetLastNoteId(){
        int id = 0;
        target = database.rawQuery("SELECT * FROM notes;", null);

        if(target != null && target.moveToFirst()) {
            target = database.query("notes", null, null, null, null, null, "id");
            target.moveToLast();

            id = target.getInt(0);

            Log.d("Database", "Last note id is " + id);

            return id;
        }else {
            return 0;
        }
    }//Получаем последний номер существующей заметки

    public static int GetLastTagId(){
        int id = 0;
        target = database.rawQuery("SELECT * FROM tags_names;", null);
        if(target != null && target.moveToFirst()){
            target = database.query("tags_names", null, null, null, null, null, "id");
            target.moveToLast();

            id = target.getInt(0);

            Log.d("Database", "Last tag id is " + id);

            return id;

        } else {
            return 0;
        }
    }//Получаем последний номер существующего тега

    public static int GetCountOfNotesInTag(int tagNumber){
        return (int)DatabaseUtils.queryNumEntries(database, "tags_table" ,"tag_id = " + String.valueOf(tagNumber));
    }//Количество заметок, которым присвоен этот тег

    public static int[] GetNoteNumbersWhatHaveThisTag(int tagNumber){
        int[] toReturn = new int[0];

        if(database == null){
            Initiate();
        }

        if(GetCountOfNotesInTag(tagNumber) > 0) {
            target = database.query("tags_table", null, "tag_id = " + String.valueOf(tagNumber), null, null, null, null);

            if(target.moveToFirst()){
                do {
                    toReturn = Arrays.copyOf(toReturn, toReturn.length + 1);
                    toReturn[toReturn.length - 1] = target.getInt(1);
                }while (target.moveToNext());
            }
        }

        return toReturn;
    }

    public static int GetLastTagTableId(){
        int id = 0;
        target = database.rawQuery("SELECT * FROM tags_table;", null);
        if(target != null && target.moveToFirst()){
            target = database.query("tags_table", null, null, null, null, null, "id");
            target.moveToLast();

            id = target.getInt(0);

            Log.d("Database", "Last tag id is " + id);

            return id;

        } else {
            return 0;
        }
    }

    public static Note GetNoteByID(int Number){

        if(database == null){
            Initiate();
        }

        target = database.query("notes", null, "id = " + String.valueOf(Number), null, null, null, null);

        if(target.moveToFirst()){
            return new Note(
                    target.getInt(0),
                    target.getFloat(1),
                    target.getString(2),
                    target.getString(3),
                    target.getString(4),
                    target.getInt(5));
        }

        return null;
    }//Извлекаем заметку по ее номеру

    //IS
    public static boolean isTagTableContains(int Number, int Tag){

        if(database == null){
            Initiate();
        }

        target = database.query("tags_table", null, "note_id = " + String.valueOf(Number), null, null, null, null);

        if(target.moveToFirst()){
            do {
                if(target.getInt(2) == Tag){
                    return true;
                }
            }while (target.moveToNext());
        }

        return false;
    }

    //CHILDS
    private static Boolean checkStatus(String Path) {
        Boolean status = false;
        File file = new File(Path);
        if(file.exists()){
            status = true;
            Log.d("Database","существует");
        }
        else{
            status = false;
            Log.d("Database","не существует");
        }
        return status;
    }//проверяет наличие БД в памяти устройства.


}
