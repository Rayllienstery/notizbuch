package com.technomagicfox.notizbuch;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.util.Arrays;

public class NotesActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    ListView NotesList;

    int tagNow = -1;
    int[] notesList = new int[0];

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notes);

        InterfaceInit();

        Permissions.CheckPermissions(getBaseContext(), NotesActivity.this);//Разрешения

    }

    @Override
    protected void onResume(){
        super.onResume();

        com.technomagicfox.notizbuch.Settings.SyncSettings(this);

        //База данных и заметки из нее
        Database.Initiate();

        Database.DeleteEmptyTags();
        NotesCore.SyncNotes();
        TagsCore.SyncTags();

        UpdateInterface();
        //Обновление списка заметок в интерфейсе
        UpdateList();
        UpdateTags();

    }

    //CORE
    public void StartActivityEditNote(int Number){

        Intent myIntent = new Intent(NotesActivity.this, EditNoteActivity.class);
        myIntent.putExtra("Number", Number);
        NotesActivity.this.startActivity(myIntent);
    }//Переход на Activity с созданием заметки

    //INTERFACE
    //Метод отвечает за обновление информации на нашем списке с отображением имени или части текста заметок.
    private void UpdateList(){
        String[] NoteNames = new String[0];

        if(tagNow == -1) {
            NoteNames = new String[NotesCore.Notes.length];//Массив имен для адаптера

            for (int x = 0; x < NotesCore.Notes.length; x++) {
                if (!NotesCore.Notes[x].title.isEmpty()) {
                    NoteNames[x] = NotesCore.Notes[x].title;
                } else {
                    //Если наша титулка пустая - возьмем текст из тела нашей заметки.
                    //Перед выводом уберем все переносы строк для нашего тела.
                    //И если строка слишком длинная - отсечем от нее часть текста.
                    if (NotesCore.Notes[x].body.length() >= 40) {
                        NotesCore.Notes[x].body = NotesCore.Notes[x].body.substring(0, 39);//Cначала отсекаем лишнее
                        NoteNames[x] = NotesCore.Notes[x].body.replaceAll("\n", " ");//После убираем перенос строки
                    } else {
                        //Перед выводом уберем все переносы строк для нашего тела.
                        NotesCore.Notes[x].body = NotesCore.Notes[x].body.replaceAll("\n", " ");
                        NoteNames[x] = NotesCore.Notes[x].body;
                    }
                }
            }
        }else {
            notesList = Database.GetNoteNumbersWhatHaveThisTag(TagsCore.Tags[tagNow].Number);
            for (int x = 0; x < notesList.length; x++) {
                Log.d("notesList", String.valueOf(notesList[x]));
            }
            Log.d("notesList", "legnth: " + notesList.length);

            NoteNames = new String[notesList.length];//Массив имен для адаптера

            for (int x = 0; x < notesList.length; x++) {
                Log.d("notesList", "x: " + String.valueOf(x));
                if (!NotesCore.Notes[notesList[x]].title.isEmpty()) {
                    NoteNames[x] = NotesCore.Notes[notesList[x]].title;
                }else {
                    if (NotesCore.Notes[notesList[x]].body.length() >= 40) {
                        NotesCore.Notes[notesList[x]].body = NotesCore.Notes[notesList[x]].body.substring(0, 39);//Cначала отсекаем лишнее
                        NoteNames[x] = NotesCore.Notes[notesList[x]].body.replaceAll("\n", " ");//После убираем перенос строки
                    } else {
                        //Перед выводом уберем все переносы строк для нашего тела.
                        NotesCore.Notes[notesList[x]].body = NotesCore.Notes[notesList[x]].body.replaceAll("\n", " ");
                        NoteNames[x] = NotesCore.Notes[notesList[x]].body;
                    }
                }
            }
        }

        try {
            ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                    R.layout.custom_list_view_2, NoteNames) {
                @Override
                public View getView(int position, View convertView, ViewGroup parent) {
                    // Get the Item from ListView
                    View view = super.getView(position, convertView, parent);

                    // Initialize a TextView for ListView each Item
                    TextView tv = (TextView) view.findViewById(android.R.id.text1);

                    // Set the text color of TextView (ListView Item)
                    if (!com.technomagicfox.notizbuch.Settings.darkMode) {
                        tv.setTextColor(Color.BLACK);
                    } else {
                        tv.setTextColor(ContextCompat.getColor(NotesActivity.this, R.color.colorPrimaryUltraLight));
                    }

                    // Generate ListView Item using TextView
                    return view;
                }
            };


            switch (Settings.textSize) {
                case 12:
                    adapter = new ArrayAdapter<String>(this,
                            R.layout.custom_list_view_0, NoteNames) {
                        @Override
                        public View getView(int position, View convertView, ViewGroup parent) {
                            // Get the Item from ListView
                            View view = super.getView(position, convertView, parent);

                            // Initialize a TextView for ListView each Item
                            TextView tv = (TextView) view.findViewById(android.R.id.text1);

                            // Set the text color of TextView (ListView Item)
                            if (!com.technomagicfox.notizbuch.Settings.darkMode) {
                                tv.setTextColor(Color.BLACK);
                            } else {
                                tv.setTextColor(ContextCompat.getColor(NotesActivity.this, R.color.colorPrimaryUltraLight));
                            }

                            // Generate ListView Item using TextView
                            return view;
                        }
                    };
                    break;
                case 14:
                    adapter = new ArrayAdapter<String>(this,
                            R.layout.custom_list_view_1, NoteNames) {
                        @Override
                        public View getView(int position, View convertView, ViewGroup parent) {
                            // Get the Item from ListView
                            View view = super.getView(position, convertView, parent);

                            // Initialize a TextView for ListView each Item
                            TextView tv = (TextView) view.findViewById(android.R.id.text1);

                            // Set the text color of TextView (ListView Item)
                            if (!com.technomagicfox.notizbuch.Settings.darkMode) {
                                tv.setTextColor(Color.BLACK);
                            } else {
                                tv.setTextColor(ContextCompat.getColor(NotesActivity.this, R.color.colorPrimaryUltraLight));
                            }

                            // Generate ListView Item using TextView
                            return view;
                        }
                    };
                    break;
                case 16:
                    adapter = new ArrayAdapter<String>(this,
                            R.layout.custom_list_view_2, NoteNames) {
                        @Override
                        public View getView(int position, View convertView, ViewGroup parent) {
                            // Get the Item from ListView
                            View view = super.getView(position, convertView, parent);

                            // Initialize a TextView for ListView each Item
                            TextView tv = (TextView) view.findViewById(android.R.id.text1);

                            // Set the text color of TextView (ListView Item)
                            if (!com.technomagicfox.notizbuch.Settings.darkMode) {
                                tv.setTextColor(Color.BLACK);
                            } else {
                                tv.setTextColor(ContextCompat.getColor(NotesActivity.this, R.color.colorPrimaryUltraLight));
                            }

                            // Generate ListView Item using TextView
                            return view;
                        }
                    };
                    break;
                case 18:
                    adapter = new ArrayAdapter<String>(this,
                            R.layout.custom_list_view_3, NoteNames) {
                        @Override
                        public View getView(int position, View convertView, ViewGroup parent) {
                            // Get the Item from ListView
                            View view = super.getView(position, convertView, parent);

                            // Initialize a TextView for ListView each Item
                            TextView tv = (TextView) view.findViewById(android.R.id.text1);

                            // Set the text color of TextView (ListView Item)
                            if (!com.technomagicfox.notizbuch.Settings.darkMode) {
                                tv.setTextColor(Color.BLACK);
                            } else {
                                tv.setTextColor(ContextCompat.getColor(NotesActivity.this, R.color.colorPrimaryUltraLight));
                            }

                            // Generate ListView Item using TextView
                            return view;
                        }
                    };
                    break;
                case 20:
                    adapter = new ArrayAdapter<String>(this,
                            R.layout.custom_list_view_4, NoteNames) {
                        @Override
                        public View getView(int position, View convertView, ViewGroup parent) {
                            // Get the Item from ListView
                            View view = super.getView(position, convertView, parent);

                            // Initialize a TextView for ListView each Item
                            TextView tv = (TextView) view.findViewById(android.R.id.text1);

                            // Set the text color of TextView (ListView Item)
                            if (!com.technomagicfox.notizbuch.Settings.darkMode) {
                                tv.setTextColor(Color.BLACK);
                            } else {
                                tv.setTextColor(ContextCompat.getColor(NotesActivity.this, R.color.colorPrimaryUltraLight));
                            }

                            // Generate ListView Item using TextView
                            return view;
                        }
                    };
                    break;
            }
            NotesList.setAdapter(adapter);
        }catch (Exception e){
            Log.e("tag", e.toString()); ;
        }
    }

    private void InterfaceInit(){
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Notizbuch");

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //-1 Означает то, что нам нужно создать новую заметку, а не отредактировать старую
                StartActivityEditNote(-1);
            }
        });


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();


        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);


        NotesList = (ListView) findViewById(R.id.NotesList);
        NotesList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View itemClicked, int position,
                                    long id) {
                if(tagNow == -1) {
                    StartActivityEditNote(NotesCore.Notes[position].id);
                }else {
                    StartActivityEditNote(NotesCore.Notes[notesList[position]].id);
                }
            }
        });
        NotesList.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                if(tagNow == -1) {
                    CallNoteDialog(position);
                }else {
                    CallNoteDialog(notesList[position]);
                }

                return true;
            }
        });


    }//Производит присвоение переменных, связанных с интерфейсом Activity

    private void UpdateInterface(){
        ListView listView = (ListView) findViewById(R.id.NotesList);

        NavigationView navView = (NavigationView) findViewById(R.id.nav_view);
        Menu menu = navView.getMenu();

        if(!com.technomagicfox.notizbuch.Settings.darkMode) {
            listView.setBackgroundColor(ContextCompat.getColor(this, R.color.colorWhite));
        }else {
            listView.setBackgroundColor(ContextCompat.getColor(this, R.color.colorPrimaryLight));
        }
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            AlertDialog.Builder BackToMenuAchtung;
            final Context context = NotesActivity.this;

            String title = "Notizbuch";
            String message = getResources().getString(R.string.exit_alert);
            String AcceptString = getResources().getString(R.string.yes);
            String RefuseString = getResources().getString(R.string.no);

            BackToMenuAchtung = new AlertDialog.Builder(context, android.R.style.Theme_Material_Dialog_NoActionBar);
            BackToMenuAchtung.setTitle(title);  // заголовок
            BackToMenuAchtung.setMessage(message); // сообщение
            BackToMenuAchtung.setPositiveButton(AcceptString, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int arg1) {
                    finish();
                }
            });
            BackToMenuAchtung.setNegativeButton(RefuseString, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int arg1) {

                }
            });
            BackToMenuAchtung.setCancelable(true);

            BackToMenuAchtung.show();
        }
    }//Кнопка "Назад"

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.notes, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            Intent intent = new Intent(this, Preferences.class);
            startActivity(intent);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {

        int id = item.getItemId();

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);

        tagNow = item.getItemId();

        UpdateList();

        return true;
    }

    private void UpdateTags() {
        NavigationView navView = (NavigationView) findViewById(R.id.nav_view);

        Menu menu = navView.getMenu();
        menu.clear();
        menu.add(0, -1, 0, getResources().getString(R.string.no_tags).toString());
        for(int x = 0 ; x < TagsCore.Tags.length; x++){
            menu.add(0, x, 0 , " - " + TagsCore.Tags[x].Name);
        }

        navView.invalidate();
    }

    private void CallNoteDialog(final int noteNumber){

        final TextView title = new TextView(NotesActivity.this);

        if(NotesCore.Notes[noteNumber].title.length() > 0) {
            if (NotesCore.Notes[noteNumber].title.length() > 25) {
                title.setText(NotesCore.Notes[noteNumber].title.substring(0, 24));
            } else {
                title.setText(NotesCore.Notes[noteNumber].title);
            }
        }else {
            String target = NotesCore.Notes[noteNumber].body;
            Log.d("target", String.valueOf(target.toCharArray()));
            char[] result = new char[0];
            int temp = 0;
            for(char x : target.toCharArray()){
                if(result.length < 25){
                    result = Arrays.copyOf(result, result.length+1);
                    result[temp] = x;
                    temp += 1;
                }else {
                    if(target.length() > result.length){

                    }else {
                        break;
                    }
                }
            }

            title.setText(String.valueOf(result).replaceAll("\n", " "));
        }


        title.setTextColor(Color.WHITE);
        title.setBackgroundResource(R.color.colorPrimary);
        title.setPadding(5, 35, 5, 35);
        title.setGravity(Gravity.CENTER);
        title.setTextSize(18);

        final String[] catNamesArray = {
                getResources().getString(R.string.share),
                getResources().getString(R.string.erase)};

        AlertDialog.Builder builder = new AlertDialog.Builder(NotesActivity.this);
        builder.setItems(catNamesArray, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which){
                            case 0:
                                Intent sendIntent = new Intent();
                                sendIntent.setAction(Intent.ACTION_SEND);
                                if(NotesCore.Notes[noteNumber].title.length() > 0){
                                    sendIntent.putExtra(Intent.EXTRA_TEXT, NotesCore.Notes[noteNumber].title + "\n" + NotesCore.Notes[noteNumber].body);
                                }else {
                                    sendIntent.putExtra(Intent.EXTRA_TEXT, NotesCore.Notes[noteNumber].body);
                                }

                                sendIntent.setType("text/plain");
                                startActivity(sendIntent);
                                break;
                            case 1:
                                Database.DeleteNote(NotesCore.Notes[noteNumber].id);
                                NotesCore.SyncNotes();
                                UpdateList();
                                break;
                        }
                    }
                });
        builder.setCustomTitle(title);

        builder.create();
        builder.show();
    }

}
