package com.technomagicfox.notizbuch;

public class Constants {

    public final static String DatabasePath = "/data/user/0/com.technomagicfox.notizbuch/";
    public final static String DatabaseName = "Database.db";

    public final static int[] TextSizes = {12,14,16,18,20};
}
