package com.technomagicfox.notizbuch;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;


public class Permissions {

    public static void CheckPermissions(Context context, Activity activity){
            //динамическое получение прав на INTERNET
        if (ContextCompat.checkSelfPermission(context, android.Manifest.permission.INTERNET)
                    == PackageManager.PERMISSION_GRANTED) {
            Log.d("INTERNET", "Permission is granted");

        } else {
            Log.d("INTERNET", "Permission is revoked");
            //запрашиваем разрешение
            ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.INTERNET}, 1);
        }

            //динамическое получение прав на READ_EXTERNAL_STORAGE
        if (ContextCompat.checkSelfPermission(context, android.Manifest.permission.READ_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_GRANTED) {
            Log.d("READ_EXTERNAL_STORAGE", "Permission is granted");

        } else {
            Log.d("READ_EXTERNAL_STORAGE", "Permission is revoked");
            //запрашиваем разрешение
            ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 1);
        }

            //динамическое получение прав на WRITE_EXTERNAL_STORAGE
        if (ContextCompat.checkSelfPermission(context, android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_GRANTED) {
            Log.d("WRITE_EXTERNAL_STORAGE", "Permission is granted");

        } else {
            Log.d("WRITE_EXTERNAL_STORAGE", "Permission is revoked");
            //запрашиваем разрешение
            ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
        }
    }
}
