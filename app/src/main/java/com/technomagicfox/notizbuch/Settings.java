package com.technomagicfox.notizbuch;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;

public class Settings {
    public static boolean darkMode = false;
    public static int textSize = 2;

    public static void SyncSettings(Context context){
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        darkMode = prefs.getBoolean("dark_theme", false);

        try {
            textSize = Constants.TextSizes[Integer.valueOf(prefs.getString("text_size", "16"))];
        }catch (Exception e){
            textSize = 16;
        }
        Log.d("myPreferences", String.valueOf(textSize));

    }
}
