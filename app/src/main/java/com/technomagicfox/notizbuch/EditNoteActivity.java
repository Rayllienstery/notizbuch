package com.technomagicfox.notizbuch;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.method.ScrollingMovementMethod;
import android.text.style.ReplacementSpan;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;

public class EditNoteActivity extends AppCompatActivity {

    EditText name;
    EditText body;
    TextView tags;

    Note note;
    TagList tagList;
    int tagNow;
    boolean[] tagsStatus;

    int Number;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_note);

        Number = getIntent().getIntExtra("Number", -1);

        InterfaceInit();//Производит присвоение переменных, связанных с интерфейсом Activity

        Database.Initiate();
        TagsCore.SyncTags();
        if(Number != -1) {
            note = Database.GetNoteByID(Number);
            tagList = Database.GetTagsListWithNoteNumber(Number);
        }else {
            tagList = new TagList(Number, new int[0]);
        }

        tagsStatus = new boolean[TagsCore.Tags.length];

        if(Number != -1) {
            for (int x = 0; x < tagList.TagsList.length; x++) {
                Log.d("TagsList", x + " = " + tagList.TagsList[x]);
                tagsStatus[x] = true;
                UpdateTags();
            }
        }

        Settings.SyncSettings(this);

        UpdateInfo();
        UpdateInterface();


    }//Входная точка

    private void UpdateTags(){
        //myTextView.setText(spanna);

        String Tags = "";
        for(int x = 0; x < tagsStatus.length; x++){

            if(tagsStatus[x]) {
                Tags += "  " + TagsCore.Tags[x].Name + "  ";
            }
        }
        if(!Tags.isEmpty()) {
            Tags = Tags.substring(0, Tags.length() - 1);
            tags.setText(Tags);

            Spannable spanna = new SpannableString(Tags);

            int positionNow = 0;
            for(int x = 0; x < tagsStatus.length; x++){

                if(tagsStatus[x]) {
                    //spanna.setSpan(new BackgroundColorSpan(getResources().getColor(R.color.colorPrimary)),positionNow, TagsCore.Tags[x].Name.length() + positionNow + 3, Spannable.SPAN_INTERMEDIATE);

                    spanna.setSpan(new RoundedBackgroundSpan(this), positionNow, TagsCore.Tags[x].Name.length() + positionNow + 3, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                    positionNow += TagsCore.Tags[x].Name.length() + 4;
                }
            }

            //spanna.setSpan(new BackgroundColorSpan(getResources().getColor(R.color.colorPrimary)),0, Tags.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

            tags.setText(spanna);
        }else {
            tags.setText(getResources().getString(R.string.add_tag));
        }
    }

    private void UpdateInfo(){
        if(Number != -1){
            name.setText(note.title);
            body.setText(note.body);
        }
    }//Обновление информации в теле макета заметки

    private void InterfaceInit(){
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if(Number == -1) {
            getSupportActionBar().setTitle(getResources().getString(R.string.add_note));
        }else {
            getSupportActionBar().setTitle(getResources().getString(R.string.edit_note));
        }

        //Кнопка "Назад"
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);


        final FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);

        if(Number == -1){
            fab.setImageDrawable(getResources().getDrawable(R.drawable.save_note));
        }

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(body.isFocusable() == true) {
                    if(!body.getText().toString().isEmpty() || !name.getText().toString().isEmpty()) {
                        WriteNote(false);
                        finish();
                    }else {
                        Toast.makeText(EditNoteActivity.this, getResources().getString(R.string.empty_note),
                                Toast.LENGTH_LONG).show();
                    }
                }else {
                    name.setFocusable(true);
                    name.setFocusableInTouchMode(true);
                    body.setFocusable(true);
                    body.setFocusableInTouchMode(true);
                    body.requestFocus();
                    fab.setImageDrawable(getResources().getDrawable(R.drawable.save_note));
                }
            }
        });

        name = (EditText) findViewById(R.id.NameText);
        body = (EditText) findViewById(R.id.BodyText);
        tags = (TextView) findViewById(R.id.TagsText);
        tags.setMovementMethod(new ScrollingMovementMethod());

        name.setTextSize(Settings.textSize);
        body.setTextSize(Settings.textSize);
        tags.setTextSize(Settings.textSize - 2);
    }//Производит присвоение переменных, связанных с интерфейсом Activity

    private void UpdateInterface(){

        //Отвечает за возможность редактировать текст
        //Если мы зашли в существующую заметку, а не создаем новую
        //То программа не даст нам редактировать ее со старта, нужно нажать на кнопку с карандашом
        if(Number != -1){
            body.setFocusable(false);
            name.setFocusable(false);

            body.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(!body.isFocusable())
                        Toast.makeText(EditNoteActivity.this, getResources().getString(R.string.is_not_focusable_error),
                                Toast.LENGTH_LONG).show();
                }
            });

            name.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(!body.isFocusable())
                        Toast.makeText(EditNoteActivity.this, getResources().getString(R.string.is_not_focusable_error),
                                Toast.LENGTH_LONG).show();
                }
            });

        }

        RelativeLayout relativeLayout = (RelativeLayout) findViewById(R.id.edit_note_background);

        if(!Settings.darkMode){
            relativeLayout.setBackgroundColor(ContextCompat.getColor(this, R.color.colorWhite));
            name.setTextColor(ContextCompat.getColor(this, R.color.colorPrimaryDark));
            body.setTextColor(ContextCompat.getColor(this, R.color.colorPrimaryDark));

            name.setHintTextColor(ContextCompat.getColor(this, R.color.colorPrimaryLight));
            body.setHintTextColor(ContextCompat.getColor(this, R.color.colorPrimaryLight));
        }else {
            relativeLayout.setBackgroundColor(ContextCompat.getColor(this, R.color.colorPrimaryLight));
            name.setTextColor(ContextCompat.getColor(this, R.color.colorPrimaryUltraLight));
            body.setTextColor(ContextCompat.getColor(this, R.color.colorPrimaryUltraLight));

            name.setHintTextColor(ContextCompat.getColor(this, R.color.colorPrimaryUltraLightHint));
            body.setHintTextColor(ContextCompat.getColor(this, R.color.colorPrimaryUltraLightHint));

            body.setLinkTextColor(ContextCompat.getColor(this, R.color.colorPrimaryUltraLight));
        }

    }

    private void WriteNote(boolean WithoutTags){
        //Добавление заметки
        String noteBody = body.getText().toString().replaceAll(" \n", "\n");
        Note note = new Note(
                Number,
                Float.parseFloat(new SimpleDateFormat("yyyyMMddHHmmss").format(Calendar.getInstance().getTime())),
                name.getText().toString(),
                noteBody,
                ";",
                0
        );

        Database.WriteNote(note);

        //Избегаем потери заметки.
        //Если у нас проблемы с распознаванием граничных условий массивов с тегами
        //Или при любой другой ошибке, мы всегда можем записать заметку без ее тегов
        if(!WithoutTags){
            WriteTags();
        }
    }//Попытка добавить заметку в базу данных

    public void WriteTags(){
        int[] tagsToWrite = new int[0];
        for (int x = 0; x < tagsStatus.length; x++) {
            if(tagsStatus[x]){
                tagsToWrite = Arrays.copyOf(tagsToWrite, tagsToWrite.length + 1);
                tagsToWrite[tagsToWrite.length - 1] = TagsCore.Tags[x].Number;
            }
        }
        if(Number != -1){
            Database.WriteTagList(new TagList(Number - 1, tagsToWrite));
        }else {
            Database.WriteTagList(new TagList(Database.GetLastNoteId() - 1, tagsToWrite));
        }
    }

    private void AddTag(){
        final AlertDialog.Builder AddTagDialogBuilder;
        final AlertDialog AddTagDialog;

        final TextView title = new TextView(EditNoteActivity.this);
        title.setText(R.string.new_tag);
        title.setTextColor(Color.WHITE);
        title.setBackgroundResource(R.color.colorPrimary);
        title.setPadding(5, 35, 5, 35);
        title.setGravity(Gravity.CENTER);
        title.setTextSize(18);



        //Инициализация
        //AddTagDialogBuilder = new AlertDialog.Builder(this);
        if(Settings.darkMode) {
            AddTagDialogBuilder = new AlertDialog.Builder(this, R.style.AppTheme_DarkDialog);
        }else {
            AddTagDialogBuilder = new AlertDialog.Builder(this);
        }
        //AddTagDialog = AddTagDialogBuilder.create();

        String[] Tags = new String[TagsCore.Tags.length];
        for(int x = 0; x < Tags.length; x++){
            Tags[x] = TagsCore.Tags[x].Name;
        }

        final boolean[] TagsStatus = tagsStatus;

        AddTagDialogBuilder.setCancelable(true);
        AddTagDialogBuilder.setCustomTitle(title)
                .setMultiChoiceItems(Tags, TagsStatus,
                        new DialogInterface.OnMultiChoiceClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog,
                                                int which, boolean isChecked) {
                                TagsStatus[which] = isChecked;
                            }
                        });
        AddTagDialogBuilder.setPositiveButton(getResources().getString(R.string.ok), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int arg1) { }
        });

        AddTagDialogBuilder.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                //Если мы добавляли тег, то мы не должны забыть увеличить и временное хранилище
                if(tagsStatus.length == TagsStatus.length) {
                    tagsStatus = TagsStatus;
                }else {
                    tagsStatus = Arrays.copyOf(TagsStatus, TagsStatus.length + 1);
                    tagsStatus[tagsStatus.length - 1] = true;
                }

                UpdateTags();

                for( int x = 0; x < tagsStatus.length; x++){
                    Log.d("TagsStatus", TagsCore.Tags[x].Number + " " + TagsCore.Tags[x].Name + " " + String.valueOf(tagsStatus[x]));
                }
            }
        });

        AddTagDialog = AddTagDialogBuilder.show();

        title.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                NewTag();
                AddTagDialog.dismiss();
            }
        });
    }

    //Нажатие на кнопку Добавить тег
    public void AddTagOnClick(View v){
        Log.d("TagsStatus", "tL.TL.l: " + tagsStatus.length);
        if(tagsStatus.length == 0 ) {
            NewTag();
        }else {
            AddTag();
        }
    }

    //Диалог добавления нового тега.
    private void NewTag(){
        AlertDialog.Builder NewTagDialog;
        final Context context = EditNoteActivity.this;

        final TextView title = new TextView(EditNoteActivity.this);
        title.setText(R.string.new_tag);
        title.setTextColor(Color.WHITE);
        title.setBackgroundResource(R.color.colorPrimary);
        title.setPadding(5, 35, 5, 35);
        title.setGravity(Gravity.CENTER);
        title.setTextSize(18);

        //Инициализация
        NewTagDialog = new AlertDialog.Builder(context);
        if(Settings.darkMode) {
            NewTagDialog = new AlertDialog.Builder(this, R.style.AppTheme_DarkDialog);
        }else {
            NewTagDialog = new AlertDialog.Builder(this);
        }

        NewTagDialog.setCustomTitle(title);
        NewTagDialog.setCancelable(true);

        LinearLayout container = new LinearLayout(this);
        container.setOrientation(LinearLayout.VERTICAL);

        final EditText TagName = new EditText(EditNoteActivity.this);

        if(Settings.darkMode){
            TagName.setTextColor(ContextCompat.getColor(this, R.color.colorPrimaryUltraLight));
            TagName.setBackgroundColor(ContextCompat.getColor(this, R.color.colorPrimaryUltraLightHint));

        }

        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT);
        lp.setMargins(20,12,20,12);
        TagName.setLayoutParams(lp);
        TagName.setGravity(android.view.Gravity.TOP|android.view.Gravity.LEFT);

        container.addView(TagName);

        NewTagDialog.setView(container);

        NewTagDialog.setPositiveButton(getResources().getString(R.string.add), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int arg1) {
                String textToAdd = TagName.getText().toString();
                if(textToAdd.length() > 0) {
                    if (textToAdd.substring(textToAdd.length() - 1) == " ") {
                        textToAdd = textToAdd.substring(0, textToAdd.length() - 1);
                    }

                    Database.AddTag(textToAdd);
                    TagsCore.SyncTags();
                    tagsStatus = Arrays.copyOf(tagsStatus, tagsStatus.length + 1);
                    tagsStatus[tagsStatus.length - 1] = true;
                }else {

                }
            }
        });

        NewTagDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                UpdateTags();
            }
        });

        NewTagDialog.show();
        //Фокусируемся на поле для ввода
        TagName.requestFocus();
        //Вызываем клавиатуру
        InputMethodManager imm = (InputMethodManager) EditNoteActivity.this.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY);
    }

    //CHILD
    @Override
    public void onBackPressed() {
        if(Number != -1) {
            if (!name.getText().toString().equals(note.title) || !body.getText().toString().equals(note.body)) {
                AchtungBeforeBackDialog();
            } else {
                WriteTags();
                finish();
            }
        }else {
            if(name.getText().toString().isEmpty() && body.getText().toString().isEmpty()) {
                finish();
            }else {
                Log.d("onBackPressed", name.getText().toString() + "\t\t" + body.getText().toString());
                AchtungBeforeBackDialog();

            }
        }
    }//Кнопка "Назад"

    private void AchtungBeforeBackDialog(){
        AlertDialog.Builder BackToMenuAchtung;
        final Context context = EditNoteActivity.this;
        if(Settings.darkMode) {
            BackToMenuAchtung = new AlertDialog.Builder(context, R.style.AppTheme_DarkDialog);
        }else {
            BackToMenuAchtung = new AlertDialog.Builder(context);
        }
        BackToMenuAchtung.setTitle(getResources().getString(R.string.exit_without_save_title));  // заголовок
        BackToMenuAchtung.setMessage(getResources().getString(R.string.exit_without_save_body)); // сообщение
        BackToMenuAchtung.setCancelable(true);

        BackToMenuAchtung.setPositiveButton(getResources().getString(R.string.yes), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int arg1) {
                finish();
            }
        });

        BackToMenuAchtung.setNegativeButton(getResources().getString(R.string.no), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int arg1) {

            }
        });

        BackToMenuAchtung.show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_edit_note, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.share) {
            Intent sendIntent = new Intent();
            sendIntent.setAction(Intent.ACTION_SEND);
            if(!name.getText().toString().isEmpty()) {
                sendIntent.putExtra(Intent.EXTRA_TEXT, name.getText().toString() + "\n" + body.getText().toString());
            }else {
                sendIntent.putExtra(Intent.EXTRA_TEXT, body.getText().toString());
            }
            sendIntent.setType("text/plain");
            startActivity(sendIntent);
            return true;

        }else if (id == R.id.delete) {
            Database.DeleteNote(getIntent().getIntExtra("Number", -1));
            finish();
            //return true;
        }else if(id == R.id.copy){
            ClipboardManager clipboard = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
            ClipData clip;
            if(name.getText().toString().length() > 0) {
                clip = ClipData.newPlainText("Note", name.getText().toString() + "\n" + body.getText().toString());
            }else {
                clip = ClipData.newPlainText("Note", body.getText().toString());
            }
            clipboard.setPrimaryClip(clip);
            Toast.makeText(this, getResources().getString(R.string.copied),
                    Toast.LENGTH_LONG).show();

        }else if (id == android.R.id.home){
            if(name.getText().toString().length() > 0 || body.getText().toString().length() > 0) {
                WriteNote(false);
            }
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

}

class RoundedBackgroundSpan extends ReplacementSpan {

    private static int CORNER_RADIUS = 8;
    private int backgroundColor = 0;
    private int textColor = 0;

    public RoundedBackgroundSpan(Context context) {
        super();
        backgroundColor = context.getResources().getColor(R.color.colorPrimary);
        textColor = context.getResources().getColor(R.color.colorPrimaryUltraLight);
    }

    public void draw(Canvas canvas, CharSequence text, int start, int end, float x, int top, int y, int bottom, Paint paint) {
        RectF rect = new RectF(x, top, x + measureText(paint, text, start, end), bottom);
        paint.setColor(backgroundColor);
        canvas.drawRoundRect(rect, CORNER_RADIUS, CORNER_RADIUS, paint);
        paint.setColor(textColor);
        canvas.drawText(text, start, end, x, y, paint);
    }

    @Override
    public int getSize(Paint paint, CharSequence text, int start, int end, Paint.FontMetricsInt fm) {
        return Math.round(paint.measureText(text, start, end));
    }

    private float measureText(Paint paint, CharSequence text, int start, int end) {
        return paint.measureText(text, start, end);
    }
}