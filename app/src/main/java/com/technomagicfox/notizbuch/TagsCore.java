package com.technomagicfox.notizbuch;

import android.util.Log;

public class TagsCore {
    public static Tag[] Tags;

    public static void SyncTags(){
        Tags = Database.GetAllTags();
    }
}

class Tag{
    int Number;
    String Name;

    public Tag(int number, String name){
        Number = number;
        Name = name;

        Log.d("Tag", Number + ": " + Name);
    }
}

class TagList{
    int NoteNumber;
    int[] TagsList;

    public boolean isEmpty(){
        if(TagsList.length == 0){
            return true;
        }else {
            return false;
        }
    }

    public TagList(int noteNumber, int[] tagsList){
        NoteNumber = noteNumber;
        TagsList = tagsList;
    }
}
