package com.technomagicfox.notizbuch;

import android.util.Log;

import java.util.Arrays;

public class NotesCore{
    public static Note[] Notes;

    public static void AddNote(Note Add){
        Arrays.copyOf(Notes, Notes.length+1);
        Notes[Notes.length-1] = Add;
    }//Добавить заметку в массив

    public static void ReadNotesFromDatabase(){

    }

    //GET
    /*
    public static Note GetNoteByID(int ID){
        for(int x = 0;x < Notes.length; x++){
            if(Notes[x].id == ID){
                return Notes[x];
            }
        }
        return null;
    }
*/
    public static void SyncNotes(){
        Notes = Database.GetAllNotes();
        Log.d("SyncNotes", Notes.length + "");
    }
}

class Note {
    public int id;
    public float date;
    public String title;
    public String body;
    public String tags;
    public int checklist;

    public Note(int Id, float Date, String Title, String Body, String Tags, int Checklist){
        id = Id;
        date = Date;
        title = Title;
        body = Body;
        tags = Tags;
        checklist = Checklist;
    }
}